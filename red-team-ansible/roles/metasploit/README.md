metasploit
==========

Role to deploy metasploit using the standard msfupdate.erb script.

Tested on CentOS 8, Debian 10, Ubuntu 18.04.

Requirements
------------

Requires python3 installed on the target server.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: metasploit_hosts
      become: yes
      roles:
         - metasploit
