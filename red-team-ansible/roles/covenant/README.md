covenant
========

Role to build the Covenant C2 docker container and run it.

Will expose the following ports:

    80   -> 80
    443  -> 443
    7443 -> 7443 (Web interface)
    8443 -> 8443

Will mount the following volume inside the container:

    /opt/Covenant/Covenant/Data:/app/Data

The git repo can be found at /opt/Covenant

Tested on Ubuntu 18.04, Debian 10. Docker currently broken on CentOS 8.

Requirements
------------

Requires python3 and docker to be installed on the target server.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: c2_hosts
      become: yes
      roles:
        - covenant
