nginx_file_server
=================

Role to deploy nginx as a static file server along with firewalld to whitelist 22, 80 and 443.

Will generate self-signed certificate, private key, CSR and will also create a PKCS12 archive (.pfx) that will get fetched to the current directory.

Tested on CentOS 8.

Requirements
------------

Requires python3 installed on the target server.

Role Variables
--------------

The two variables required for this role are:

    - "private_key_password" used to password-protect the PKCS12 archive containing the Private Key and the Certificate

Can be set in the inventory as follows:

    [nginx_file_server_hosts]
    <ip_address/hostname> ansible_user=<user> ansible_ssh_pass=<pass> ansible_ssh_private_key_file= ansible_sudo_pass=<sudo-pass>

    [nginx_file_server_hosts:vars]
    private_key_password="<password_for_tls_certificate>"

Or it can be set in the playbook as follows:

    - hosts: nginx_file_server_hosts
      become: yes
      roles:
        - redirector
      vars:
        - private_key_password: "<password_for_tls_certificate>"

Example Playbook
----------------

    - hosts: nginx_file_server_hosts
      become: yes
      roles:
         - nginx_file_server
