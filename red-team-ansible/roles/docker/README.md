docker
======

Role to install docker.

Will install the docker-ce repositories, fetch the GPG or APT key and will install docker-ce and it's dependencies.

Will enable and start the docker container, but will not create a dedicated user for it.

Tested on Ubuntu 18.04, Debian 10. Docker currently broken on CentOS 8.

Requirements
------------

Requires python3 installed on the target server.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: docker_hosts
      become: yes
      roles:
         - docker
