configure_firewalld
===================

Role to deploy and configure firewalld.

Will set default zone to "drop" and whitelist IP addresses in the "trusted" zone.

Tested on CentOS 8, Ubuntu 18.04, Debian 10.

NOTE: Debian 10 Stretch package iptables=1.8.2 is bugged. This playbook adds the buster-backports repository and installs iptables=1.8.3-2

Requirements
------------

Requires python3 installed on the target server.

Role Variables
--------------

The two variables required for this role are:

    - "whitelisted_ip_address" used to whitelist IP addresses by adding them to firewalld's "trusted" zone
    - "ansible_client_ip_address" used to whitelist current IP address of the machine used to provision the target (esentially, to not ban yourself out)

Can be set in the inventory as follows:

    [configure_firewalld_hosts]
    <ip_address/hostname> ansible_user=<user> ansible_ssh_pass=<pass> ansible_ssh_private_key_file= ansible_sudo_pass=<sudo-pass>

    [configure_firewalld_hosts:vars]
    whitelisted_ip_address=["192.168.1.1", "192.168.1.2"]
    ansible_client_ip_address="192.168.1.3"

Or it can be set in the playbook as follows:

    - hosts: configure_firewalld_hosts
      become: yes
      roles:
        - configure_firewalld
      vars:
        - whiteslited_ip_addresses:
            - "192.168.1.1"
            - "192.168.1.2"
        - ansible_client_ip_address:
            - "192.168.1.3"


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: configure_firewalld_hosts
      become: yes
      roles:
        - configure_firewalld
