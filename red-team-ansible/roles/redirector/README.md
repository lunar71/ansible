redirector
==========

Role to deploy an nginx traffic redirector using proxy_pass.

Will generate self-signed certificate, private key, CSR and will also create a PKCS12 archive (.pfx) that will get fetched to the current directory.

Will redirect the following protocols/ports:

Stream proxy:

    2121 -> 21   (FTP)
    2222 -> 22   (SSH)
    53   -> 53   (DNS)

HTTP proxy:

    80   -> 80   (HTTP)
    8080 -> 8080 (HTTP)
    443  -> 443  (HTTPS)
    8443 -> 8443 (HTTPS)

Tested on Ubuntu 18.04, CentOS 8, Debian 10.

Requirements
------------

Requires python3 installed on the target server.

Role Variables
--------------

The two variables required for this role are:

    - "private_key_password" used to password-protect the PKCS12 archive containing the Private Key and the Certificate
    - "c2_ipaddress" used to set the proxy target IP address/domain.

Can be set in the inventory as follows:

    [redirector_hosts]
    <ip_address/hostname> ansible_user=<user> ansible_ssh_pass=<pass> ansible_ssh_private_key_file= ansible_sudo_pass=<sudo-pass>

    [redirector_hosts:vars]
    private_key_password="<password_for_tls_certificate>"
    c2_ipaddress="<ip_address_of_c2_to_redirect_to>"

Or it can be set in the playbook as follows:

    - hosts: redirector_hosts
      become: yes
      roles:
        - redirector
      vars:
        - private_key_password: "<password_for_tls_certificate>"
        - c2_ipaddress: "<ip_address_of_c2_to_redirect_to>"

Example Playbook
----------------

    - hosts: redirector_hosts
      become: yes
      roles:
        - redirector
